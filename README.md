# TimerKit

[![CI Status](http://img.shields.io/travis/srstanic/TimerKit.svg?style=flat)](https://travis-ci.org/srstanic/TimerKit)
[![Version](https://img.shields.io/cocoapods/v/TimerKit.svg?style=flat)](http://cocoapods.org/pods/TimerKit)
[![License](https://img.shields.io/cocoapods/l/TimerKit.svg?style=flat)](http://cocoapods.org/pods/TimerKit)
[![Platform](https://img.shields.io/cocoapods/p/TimerKit.svg?style=flat)](http://cocoapods.org/pods/TimerKit)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

TimerKit is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'TimerKit'
```

## Author

srstanic, srstanic@gmail.com

## License

TimerKit is available under the MIT license. See the LICENSE file for more info.
